package com.sharma.volleyexample;

import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    BroadcastReceiver onComplete = new BroadcastReceiver() {

        public void onReceive(Context ctxt, Intent intent) {

            // get the refid from the download manager
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

// show a notification
            Log.e("INSIDE", "" + referenceId);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(MainActivity.this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("File")
                            .setContentText("Download completed");


            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.notify(0, mBuilder.build());
        }
    };

    /*private void uploadingTask() {
        mNotifyManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());
        mBuilder.setContentTitle("Upload image")
                .setContentText("")
                .setAutoCancel(false)
                .setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT))
                .setSmallIcon(R.drawable.ic_launcher);
        MultiPartRequest jsonRequest = new SimpleMultiPartRequest(Request.Method.POST, "http://posttestserver.com/post.php?dir=123",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(getClass().getName(), response);
                        mBuilder.setContentText("Upload image complete!");
                        mNotifyManager.notify(AppSettings.SEND_DATA_NOTIF, mBuilder.build());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(getClass().getName(), error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return mParams;
            }

        };
        jsonRequest.addFile("images", (IMAGE_PATH);
        jsonRequest.setFixedStreamingMode(true);
        jsonRequest.setShouldCache(false);
        jsonRequest.setShouldRetryServerErrors(true);
        jsonRequest.setOnProgressListener(new Response.ProgressListener() {
            @Override
            public void onProgress(long transferredBytes, long totalSize) {
                final int percentage = (int) ((transferredBytes / ((float) totalSize)) * 100);
                mBuilder.setProgress(100, percentage, false);
                mNotifyManager.notify(AppSettings.SEND_DATA_NOTIF, mBuilder.build());
//freeze system UI
            }
        });
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mRequestQueue.add(jsonRequest);
        mRequestQueue.start();
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registerReceiver(onComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        //AndroidNetworking.download()

        //uploadingTask();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
    }
}
