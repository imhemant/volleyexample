package com.sharma.volleyexample.volley;


import com.android.volley.Request;

import java.util.Map;

/**
 * Created by hemant.
 * Date: 31/8/18
 */

public interface VolleyRequestBuilder {

    VolleyRequestBuilder setPriority(Request.Priority priority);

    VolleyRequestBuilder setBodyContentType(String contentType);

    VolleyRequestBuilder setParamsEncoding(String paramsEncoding);

    VolleyRequestBuilder addHeaders(String key, String value);

    VolleyRequestBuilder addHeaders(Map<String, String> headerMap);

    VolleyRequestBuilder addParameter(String key, String value);

    VolleyRequestBuilder addParameter(Map<String, String> paramMap);

    VolleyRequestBuilder setTag(String tag);

    VolleyRequestBuilder addRetryTime(int retryTime);

}
