package com.sharma.volleyexample.volley;

import com.android.volley.VolleyError;

import org.json.JSONArray;

/**
 * Created by hemant.
 * Date: 31/8/18
 */

public interface VolleyJsonArrayListener {

    void onVolleyJsonArray(JSONArray jsonArray);

    void onVolleyError(VolleyError error);
}
