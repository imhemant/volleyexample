package com.sharma.volleyexample.volley;

/**
 * Created by hemant.
 * Date: 31/8/18
 */

public interface RequestType {
    int SIMPLE = 0;
    int MULTIPART = 1;
    int DOWNLOAD = 2;
}
