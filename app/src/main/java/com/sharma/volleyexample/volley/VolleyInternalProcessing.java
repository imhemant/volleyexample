package com.sharma.volleyexample.volley;

import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

/**
 * Created by hemant.
 * Date: 31/8/18
 */

class VolleyInternalProcessing {

    private static VolleyInternalProcessing sInstance;

    private VolleyInternalProcessing() {
    }

    synchronized static VolleyInternalProcessing getInstance() {
        if (sInstance == null) {
            sInstance = new VolleyInternalProcessing();
        }
        return sInstance;
    }

    void execute(VolleyRequest request) {

        switch (request.getRequestType()) {

            case RequestType.DOWNLOAD:
              /*  DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri =Uri.parse("http://techslides.com/demos/sample-videos/small.mp4");
                DownloadManager.Request downloadRequest = new DownloadManager.Request(uri);
                downloadRequest.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                downloadRequest.setAllowedOverRoaming(false);
                downloadRequest.setTitle("Test File Download");
                downloadRequest.setDescription("Downloading Sample.png");
                downloadRequest.setVisibleInDownloadsUi(true);
                downloadRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/DownloadTest/");
                downloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                assert downloadManager != null;
                long ref = downloadManager.enqueue(downloadRequest);
                Log.e("Test", ref+"");*/
                break;

            case RequestType.MULTIPART:
                VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(request.getmMethod(), request.getmUrl(), new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(@NonNull NetworkResponse response) {
                        String resultResponse = new String(response.data);
                        Log.e(request.getmTag(), "onResponse: " + resultResponse);
                        request.onSuccessResponse(resultResponse);
                    }
                }, error -> request.onErrorResponse(error)) {
                    @NonNull
                    @Override
                    public Map<String, String> getHeaders() {
                        return request.getmHeadersMap();
                    }

                    @NonNull
                    @Override
                    protected Map<String, String> getParams() {
                        return request.getmParameterMap();
                    }

                    @NonNull
                    @Override
                    protected Map<String, DataPart> getByteData() {
                        return request.getmDataParamsMap();
                    }

                    @Override
                    public Priority getPriority() {
                        return request.getmPriority();
                    }

                    @NonNull
                    @Override
                    public String getBodyContentType() {
                        return request.getmContentType().isEmpty() ? super.getBodyContentType() : request.getmContentType();
                    }

                    @Override
                    protected String getParamsEncoding() {
                        return request.getmParamsEncoding().isEmpty() ? super.getParamsEncoding() : request.getmParamsEncoding();
                    }
                };
                VolleyRequestQueue.getInstance().addToRequestQueue(multipartRequest, request.getmTag());
                multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                        request.getmRetryTime(), DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                break;

            case RequestType.SIMPLE:
                StringRequest stringRequest = new StringRequest(request.getmMethod(), request.getmUrl(),
                        response -> {
                            Log.e(request.getmTag(), "onResponse: " + response);
                            request.onSuccessResponse(response);
                        },
                        request::onErrorResponse) {

                    @NonNull
                    @Override
                    public Map<String, String> getHeaders() {
                        return request.getmHeadersMap();
                    }

                    @NonNull
                    @Override
                    protected Map<String, String> getParams() {
                        return request.getmParameterMap();
                    }

                    @Override
                    public Priority getPriority() {
                        return request.getmPriority();
                    }

                    @NonNull
                    @Override
                    public String getBodyContentType() {
                        return request.getmContentType().isEmpty() ? super.getBodyContentType() : request.getmContentType();
                    }

                    @Override
                    protected String getParamsEncoding() {
                        return request.getmParamsEncoding().isEmpty() ? super.getParamsEncoding() : request.getmParamsEncoding();
                    }
                };

                VolleyRequestQueue.getInstance().addToRequestQueue(stringRequest, request.getmTag());
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        request.getmRetryTime(), DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                break;
        }
    }

}
