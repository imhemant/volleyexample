package com.sharma.volleyexample.volley;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by hemant.
 * Date: 31/8/18
 */

public interface VolleyJsonObjectListener {

    void onVolleyJsonObject(JSONObject jsonObject);

    void onVolleyError(VolleyError error);
}
