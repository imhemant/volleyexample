package com.sharma.volleyexample.volley;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.sharma.volleyexample.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hemant.
 * Date: 31/8/18
 */

public class VolleyRequest {

    private int requestType;
    private int mMethod;
    private Request.Priority mPriority;
    private String mUrl;
    private String mTag;
    private int mRetryTime;

    private String mContentType = "";
    private String mParamsEncoding = "";
    private Map<String, String> mHeadersMap;
    private Map<String, String> mParameterMap;
    private Map<String, VolleyMultipartRequest.DataPart> mDataParamsMap;

    private VolleyJsonArrayListener mJSONArrayRequestListener;
    private VolleyJsonObjectListener mJSONObjectRequestListener;
    private VolleyResponseListener mVolleyResponseListener;

    public VolleyRequest(GetRequestBuilder builder) {
        requestType = RequestType.SIMPLE;
        this.mRetryTime = builder.mRetryTime;
        this.mContentType = builder.mContentType;
        this.mParamsEncoding = builder.mParamsEncoding;
        this.mMethod = builder.mMethod;
        this.mPriority = builder.mPriority;
        this.mUrl = builder.mUrl;
        this.mTag = builder.mTag;
        this.mHeadersMap = builder.mHeadersMap;
        this.mParameterMap = builder.mParameterMap;
    }

    public VolleyRequest(PostRequestBuilder builder) {
        requestType = RequestType.SIMPLE;
        this.mRetryTime = builder.mRetryTime;
        this.mContentType = builder.mContentType;
        this.mParamsEncoding = builder.mParamsEncoding;
        this.mMethod = builder.mMethod;
        this.mPriority = builder.mPriority;
        this.mUrl = builder.mUrl;
        this.mTag = builder.mTag;
        this.mHeadersMap = builder.mHeadersMap;
        this.mParameterMap = builder.mParameterMap;
    }

    public VolleyRequest(PostMultipartBuilder builder) {
        requestType = RequestType.MULTIPART;
        this.mRetryTime = builder.mRetryTime;
        this.mContentType = builder.mContentType;
        this.mParamsEncoding = builder.mParamsEncoding;
        this.mMethod = builder.mMethod;
        this.mPriority = builder.mPriority;
        this.mUrl = builder.mUrl;
        this.mTag = builder.mTag;
        this.mHeadersMap = builder.mHeadersMap;
        this.mParameterMap = builder.mParameterMap;
        this.mDataParamsMap = builder.mDataParamsMap;
    }

    @NonNull
    public static String volleyErrorHandle(@NonNull Activity activity, VolleyError error) {
        NetworkResponse networkResponse = error.networkResponse;
        String errorMessage = "";
        if (networkResponse == null) {
            if (error.getClass().equals(TimeoutError.class)) {
                errorMessage = activity.getString(R.string.alert_volley_timeout);
                return errorMessage;
            } else if (error.getClass().equals(NoConnectionError.class)) {
                errorMessage = activity.getString(R.string.alert_volley_server_error);
                return errorMessage;
            } else return errorMessage;
        } else {
            String result = new String(networkResponse.data);
            try {
                JSONObject response = new JSONObject(result);
                String status = response.getString("responseCode");
                String message = response.getString("message");

                Log.e("Error Status", "" + status);
                Log.e("Error Message", message);

                if (status.equals("300")) {
                    errorMessage = message;
                } else if (networkResponse.statusCode == 404) {
                    errorMessage = "Resource not found";
                } else if (networkResponse.statusCode == 500) {
                    errorMessage = message + "Oops! Something went wrong";
                } else {
                    errorMessage = ServerResponseCode.getMessageCode(networkResponse.statusCode);
                }

            } catch (JSONException e) {
                errorMessage = "";
                e.printStackTrace();
            }
        }
        return errorMessage;
    }

    public int getRequestType() {
        return requestType;
    }

    int getmMethod() {
        return mMethod;
    }

    Request.Priority getmPriority() {
        return mPriority;
    }

    String getmUrl() {
        return mUrl;
    }

    String getmTag() {
        return mTag;
    }

    int getmRetryTime() {
        return mRetryTime;
    }

    String getmContentType() {
        return mContentType;
    }

    String getmParamsEncoding() {
        return mParamsEncoding;
    }

    Map<String, String> getmHeadersMap() {
        return mHeadersMap;
    }

    Map<String, String> getmParameterMap() {
        return mParameterMap;
    }

    Map<String, VolleyMultipartRequest.DataPart> getmDataParamsMap() {
        return mDataParamsMap;
    }

    public void getAsJSONObject(VolleyJsonObjectListener listener) {
        this.mJSONObjectRequestListener = listener;
        VolleyInternalProcessing.getInstance().execute(this);
    }

    public void getAsJSONArray(VolleyJsonArrayListener listener) {
        this.mJSONArrayRequestListener = listener;
        VolleyInternalProcessing.getInstance().execute(this);
    }

    public void getAsResponse(VolleyResponseListener listener) {
        this.mVolleyResponseListener = listener;
        VolleyInternalProcessing.getInstance().execute(this);
    }

    void onSuccessResponse(Object response) {
        if (mJSONObjectRequestListener != null) {
            mJSONObjectRequestListener.onVolleyJsonObject((JSONObject) response);
        } else if (mJSONArrayRequestListener != null) {
            mJSONArrayRequestListener.onVolleyJsonArray((JSONArray) response);
        } else if (mVolleyResponseListener != null) {
            mVolleyResponseListener.onVolleyResponse((String) response);
        }
    }

    void onErrorResponse(VolleyError error) {
        if (mJSONObjectRequestListener != null) {
            mJSONObjectRequestListener.onVolleyError(error);
        } else if (mJSONArrayRequestListener != null) {
            mJSONArrayRequestListener.onVolleyError(error);
        } else if (mVolleyResponseListener != null) {
            mVolleyResponseListener.onVolleyError(error);
        }
    }

    public static class GetRequestBuilder<T extends GetRequestBuilder> implements VolleyRequestBuilder {

        private int mMethod = Request.Method.GET;
        private String mUrl;
        private String mTag;
        private Request.Priority mPriority = Request.Priority.NORMAL;
        private String mContentType = "";
        private String mParamsEncoding = "";
        private int mRetryTime = 30000;
        private HashMap<String, String> mHeadersMap = new HashMap<>();
        private HashMap<String, String> mParameterMap = new HashMap<>();

        public GetRequestBuilder(String url) {
            this.mUrl = url;
            this.mMethod = Request.Method.GET;
        }

        public GetRequestBuilder(String url, int method) {
            this.mUrl = url;
            this.mMethod = method;
        }

        @Override
        public T setTag(String tag) {
            mTag = tag;
            return (T) this;
        }

        @Override
        public T addRetryTime(int mRetryTime) {
            this.mRetryTime = mRetryTime;
            return (T) this;
        }

        @Override
        public T setPriority(Request.Priority priority) {
            mPriority = priority;
            return (T) this;
        }

        @Override
        public T setBodyContentType(String contentType) {
            mContentType = contentType;
            return (T) this;
        }

        @Override
        public T setParamsEncoding(String paramsEncoding) {
            mParamsEncoding = paramsEncoding;
            return (T) this;
        }

        @Override
        public T addHeaders(String key, String value) {
            assert key != null && value != null;
            mHeadersMap.put(key, value);
            return (T) this;
        }

        @Override
        public T addHeaders(Map<String, String> headerMap) {
            if (headerMap != null) {
                for (HashMap.Entry<String, String> entry : headerMap.entrySet()) {
                    addHeaders(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        @Override
        public T addParameter(String key, String value) {
            assert key != null && value != null;
            mParameterMap.put(key, value);
            return (T) this;
        }


        @Override
        public T addParameter(Map<String, String> parameterMap) {
            if (parameterMap != null) {
                for (HashMap.Entry<String, String> entry : parameterMap.entrySet()) {
                    addParameter(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        public VolleyRequest build() {
            return new VolleyRequest(this);
        }
    }

    public static class PostRequestBuilder<T extends PostRequestBuilder> implements VolleyRequestBuilder {

        private Request.Priority mPriority = Request.Priority.NORMAL;
        private int mMethod = Request.Method.POST;
        private String mUrl;
        private String mTag;
        private int mRetryTime = 30000;
        private String mContentType = "";
        private String mParamsEncoding = "";
        private HashMap<String, String> mHeadersMap = new HashMap<>();
        private HashMap<String, String> mParameterMap = new HashMap<>();

        public PostRequestBuilder(String url) {
            this.mUrl = url;
            this.mMethod = Request.Method.POST;
        }

        public PostRequestBuilder(String url, int method) {
            this.mUrl = url;
            this.mMethod = method;
        }

        @Override
        public T setTag(String tag) {
            mTag = tag;
            return (T) this;
        }

        @Override
        public T addRetryTime(int mRetryTime) {
            this.mRetryTime = mRetryTime;
            return (T) this;
        }

        @Override
        public T setPriority(Request.Priority priority) {
            mPriority = priority;
            return (T) this;
        }

        @Override
        public T setBodyContentType(String contentType) {
            mContentType = contentType;
            return (T) this;
        }

        @Override
        public T setParamsEncoding(String paramsEncoding) {
            mParamsEncoding = paramsEncoding;
            return (T) this;
        }

        @Override
        public T addParameter(String key, String value) {
            assert key != null && value != null;
            mParameterMap.put(key, value);
            return (T) this;
        }

        @Override
        public T addParameter(Map<String, String> parameterMap) {
            if (parameterMap != null) {
                for (HashMap.Entry<String, String> entry : parameterMap.entrySet()) {
                    addParameter(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }


        @Override
        public T addHeaders(String key, String value) {
            assert key != null && value != null;
            mHeadersMap.put(key, value);
            return (T) this;
        }

        @Override
        public T addHeaders(Map<String, String> headerMap) {
            if (headerMap != null) {
                for (HashMap.Entry<String, String> entry : headerMap.entrySet()) {
                    addHeaders(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        public VolleyRequest build() {
            return new VolleyRequest(this);
        }
    }

    public static class PostMultipartBuilder<T extends PostMultipartBuilder> implements VolleyRequestBuilder {

        private Request.Priority mPriority = Request.Priority.NORMAL;
        private int mMethod = Request.Method.POST;
        private String mUrl;
        private String mTag;
        private int mRetryTime = 30000;
        private String mContentType = "";
        private String mParamsEncoding = "";
        private Map<String, String> mHeadersMap = new HashMap<>();
        private Map<String, String> mParameterMap = new HashMap<>();
        private Map<String, VolleyMultipartRequest.DataPart> mDataParamsMap = new HashMap<>();

        public PostMultipartBuilder(String url) {
            this.mUrl = url;
            this.mMethod = Request.Method.POST;
        }

        public PostMultipartBuilder(String url, int method) {
            this.mUrl = url;
            this.mMethod = method;
        }

        @Override
        public T setTag(String tag) {
            mTag = tag;
            return (T) this;
        }

        @Override
        public T addRetryTime(int mRetryTime) {
            this.mRetryTime = mRetryTime;
            return (T) this;
        }

        @Override
        public T setPriority(Request.Priority priority) {
            mPriority = priority;
            return (T) this;
        }

        @Override
        public T setBodyContentType(String contentType) {
            mContentType = contentType;
            return (T) this;
        }

        @Override
        public T setParamsEncoding(String paramsEncoding) {
            mParamsEncoding = paramsEncoding;
            return (T) this;
        }

        @Override
        public T addParameter(String key, String value) {
            assert key != null && value != null;
            mParameterMap.put(key, value);
            return (T) this;
        }

        @Override
        public T addParameter(Map<String, String> parameterMap) {
            if (parameterMap != null) {
                for (HashMap.Entry<String, String> entry : parameterMap.entrySet()) {
                    addParameter(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        public T addMultipartParameter(String key, VolleyMultipartRequest.DataPart data) {
            assert key != null && data != null;
            mDataParamsMap.put(key, data);
            return (T) this;
        }

        public T addMultipartParameter(Map<String, VolleyMultipartRequest.DataPart> mDataMap) {
            if (mDataMap != null) {
                for (HashMap.Entry<String, VolleyMultipartRequest.DataPart> entry : mDataMap.entrySet()) {
                    addMultipartParameter(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        @Override
        public T addHeaders(String key, String value) {
            assert key != null && value != null;
            mHeadersMap.put(key, value);
            return (T) this;
        }

        @Override
        public T addHeaders(Map<String, String> headerMap) {
            if (headerMap != null) {
                for (HashMap.Entry<String, String> entry : headerMap.entrySet()) {
                    addHeaders(entry.getKey(), entry.getValue());
                }
            }
            return (T) this;
        }

        public VolleyRequest build() {
            return new VolleyRequest(this);
        }
    }
}
