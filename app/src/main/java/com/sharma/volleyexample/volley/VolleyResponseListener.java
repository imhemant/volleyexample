package com.sharma.volleyexample.volley;

import com.android.volley.VolleyError;

/**
 * Created by hemant.
 * Date: 31/8/18
 */

public interface VolleyResponseListener {

    void onVolleyResponse(String response);

    void onVolleyError(VolleyError error);
}
