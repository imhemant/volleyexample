package com.sharma.volleyexample.volley;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by hemant.
 * Date: 31/8/18
 */

public class VolleyRequestQueue {

    private static final String TAG = VolleyRequestQueue.class.getName();
    private static RequestQueue mRequestQueue;
    private static VolleyRequestQueue sInstance;


    static void initialize(Context context) {
        getRequestQueue(context);
    }

    synchronized static VolleyRequestQueue getInstance() {
        if (sInstance == null) {
            sInstance = new VolleyRequestQueue();
        }
        return sInstance;
    }

    /**
     * Get current request queue.
     *
     * @return RequestQueue
     */
    private static synchronized RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }

    <T> void addToRequestQueue(@NonNull Request<T> req, String tag) {
        if (mRequestQueue != null) {
            // set the default tag if tag is empty
            req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
            mRequestQueue.add(req);
        }
    }

    public <T> void addToRequestQueue(@NonNull Request<T> req) {
        if (mRequestQueue != null) {
            req.setTag(TAG);
            mRequestQueue.add(req);
        }
    }

    void cancelRequests(@NonNull Object tag) {
        if (mRequestQueue != null) {
            Log.e(TAG, "cancel api");
            mRequestQueue.cancelAll(tag);
        }
    }

}
