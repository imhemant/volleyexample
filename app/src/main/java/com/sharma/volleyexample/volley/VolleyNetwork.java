package com.sharma.volleyexample.volley;

import android.content.Context;

/**
 * Created by hemant.
 * Date: 31/8/18
 */
public class VolleyNetwork {

    /**
     * private constructor to prevent instantiation of this class
     */
    private VolleyNetwork() {
    }

    /**
     * Initializes VolleyNetwork with the default config.
     *
     * @param context The context
     */
    public static void initialize(Context context) {
        VolleyRequestQueue.initialize(context);
    }


    /**
     * Method to make GET request
     *
     * @param url The url on which request is to be made
     * @return The GetRequestBuilder
     */
    public static VolleyRequest.GetRequestBuilder get(String url) {
        return new VolleyRequest.GetRequestBuilder(url);
    }

    /**
     * Method to make POST request
     *
     * @param url The url on which request is to be made
     * @return The PostRequestBuilder
     */
    public static VolleyRequest.PostRequestBuilder post(String url) {
        return new VolleyRequest.PostRequestBuilder(url);
    }

    /**
     * Method to make POST Multipart request
     *
     * @param url The url on which request is to be made
     * @return The PostMultipartBuilder
     */
    public static VolleyRequest.PostMultipartBuilder postMultipart(String url) {
        return new VolleyRequest.PostMultipartBuilder(url);
    }

    /**
     * Method to make upload request
     *
     * @param url The url on which request is to be made
     * @return The PostMultipartBuilder
     */
    public static VolleyRequest.PostMultipartBuilder upload(String url) {
        return new VolleyRequest.PostMultipartBuilder(url);
    }

    /**
     * Method to make upload request
     *
     * @param url The url on which request is to be made
     * @return The PostMultipartBuilder
     */
    public static VolleyRequest.PostMultipartBuilder download(String url) {
        return new VolleyRequest.PostMultipartBuilder(url);
    }

    /**
     * Method to cancel requests with the given tag
     *
     * @param tag The tag with which requests are to be cancelled
     */
    public static void cancelRequests(Object tag) {
        VolleyRequestQueue.getInstance().cancelRequests(tag);
    }


}
